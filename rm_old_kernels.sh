#!/bin/bash

if [ $# -lt 1 ]; then
    printf 'Usage: '`basename $0`' <command> \n\n'
    printf 'Avaliable commands: \n'
    printf '   list       Show the list of old packages \n'
    printf '   purge      Uninstall old packages \n'
    exit 1
fi

key="$1"

OLD_PKG=$(dpkg --get-selections |
        egrep 'linux-(image|headers-3|signed-image-3)' |
        grep -v $(uname -r | cut -d '-' -f 1,2) |
        cut -d$'\t' -f 1)

case $key in
	list)
	echo $OLD_PKG | tr " " "\n"
	;;
	purge)
	sudo apt-get purge $OLD_PKG
	;;
	*)
	echo $1 is uknown command
	;;
esac
