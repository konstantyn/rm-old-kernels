#!/bin/bash

# compare linux-image-extra-3.13.0-57-generic 3.13.0-58 > 1
# compare linux-image-extra-3.13.0-58-generic 3.13.0-58 > 0
# compare linux-image-extra-3.13.0-59-generic 3.13.0-58 > 0
function compare {
  REL=`echo $1 | grep -oP '\d[.]\d+[.]\d+[-]\d+'`
  if [[ "$REL" < "$2" ]]; then
    echo 1
  else
    echo 0
  fi
}

ALL=`cat all | cut -d$'\t' -f 1`
CUR='3.13.0-58'

echo $ALL | tr " " "\n"
printf '\n'

# array2=( $( for i in ${array[@]} ; do echo $i ; done ) )

# OLD=`for i in $ALL ; do if [ "`compare $i $CUR`" = 1 ] ; then echo $i ; fi done`
# echo $OLD
for i in $ALL ; do if [ "`compare $i $CUR`" = 1 ] ; then echo $i ; fi done
